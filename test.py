from configparser import ConfigParser
from itertools import chain


parser = ConfigParser()
with open("smobi_host.ini") as lines:
    lines = chain(("[name]",), lines)  # This line does the trick.
    parser.read_file(lines)
    print(parser)